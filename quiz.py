# coding=utf-8
import operator
import webbrowser
import time

artiesten = {"Indianizer":0,"Moon Moon Moon":0,"Mercy John":0,"Glenn Foreman":0,"Clittenband":0,"George Kush Collective":0,"Niko":0,"Lamin Kuyateh":0,"Mensen Zeggen Dingen":0,"Rene Oskam":0,"De Nieuwe Oost":0,"Mikaela Burch":0,"Sabba":0,"CTF":0,"Studio Figur":0,"NNT":0,"Tom Lash":0,"Go Short":0,"KNOT":0,"Clarinde":0,}
locaties = {"Indianizer":"Jodenberg+2+Nijmegen","Moon Moon Moon":"Van+der+Brugghenstraat+5B+Nijmegen","Mercy John":"Groesbeeksedwarsweg+100+Nijmegen","Glenn Foreman":"Oranjesingel+70+Nijmegen","Clittenband":"Sint+Annastraat+102+Nijmegen","George Kush Collective":"Stikke+Hezelstraat+1+Nijmegen","Niko":"Dobbelmannweg+1+Nijmegen","Lamin Kuyateh":"Uranusstraat+31+Nijmegen","Mensen Zeggen Dingen":"Sint+Canisiussingel+25+Nijmegen","Rene Oskam":"Groesbeekseweg+147+Nijmegen","De Nieuwe Oost":"Pegasusplaats+214+Nijmegen","Mikaela Burch":"Acaciastraat+9+Nijmegen","Sabba":"Sint+Annastraat+123+Nijmegen","CTF":"Waalbandijk+79+Nijmegen","Studio Figur":"Van+Oldenbarneveltstraat+63A+Nijmegen","NNT":"Schoolstraat+61+Nijmegen","Tom Lash":"Groesbeekseweg+51+Nijmegen","Go Short":"In+de+Betouwstraat+11A+Nijmegen","KNOT":"Oranjesingel+43+nijmegen","Clarinde":"Groesbeekseweg+147+Nijmegen",}
sitelinks = {"Indianizer":"indianizer-2","Moon Moon Moon":"moon-moon-moon","Mercy John":"mercy-john","Glenn Foreman":"glenn-foreman-3","Clittenband":"clittenband","George Kush Collective":"george-kush-collective","Niko":"niko","Lamin Kuyateh":"lamin-kuyateh","Mensen Zeggen Dingen":"mensen-zeggen-dingen-4","Rene Oskam":"rene-oskam","De Nieuwe Oost":"nieuwe-oost-wintertuin","Mikaela Burch":"sam-humes-paul-russell","Sabba":"sabba","CTF":"cafe-theater-festival-3","Studio Figur":"studio-figur","NNT":"nnt-life-4-ever-rene-donders","Tom Lash":"tom-lash-3","Go Short":"go-short-international-short-film-festival-nijmegen","KNOT":"knot-collective","Clarinde":"clarinde-wesselink",}


wordart1 = """
                                                                                                   
                                                                                                   
  .--.--.       ___                        ,-.               ,---,.                        ___     
 /  /    '.   ,--.'|_                  ,--/ /|             ,'  .' |                      ,--.'|_   
|  :  /`. /   |  | :,'          ,--, ,--. :/ |           ,---.'   |                      |  | :,'  
;  |  |--`    :  : ' :        ,'_ /| :  : ' /            |   |   .'            .--.--.   :  : ' :  
|  :  ;_    .;__,'  /    .--. |  | : |  '  /    ,--.--.  :   :  :     ,---.   /  /    '.;__,'  /   
 \  \    `. |  |   |   ,'_ /| :  . | '  |  :   /       \ :   |  |-,  /     \ |  :  /`./|  |   |    
  `----.   \:__,'| :   |  ' | |  . . |  |   \ .--.  .-. ||   :  ;/| /    /  ||  :  ;_  :__,'| :    
  __ \  \  |  '  : |__ |  | ' |  | | '  : |. \ \__\/: . .|   |   .'.    ' / | \  \    `. '  : |__  
 /  /`--'  /  |  | '.'|:  | : ;  ; | |  | ' \ \," .--.; |'   :  '  '   ;   /|  `----.   \|  | '.'| 
'--'.     /   ;  :    ;'  :  `--'   \\'  : |--'/  /  ,.  ||   |  |  '   |  / | /  /`--'  /;  :    ; 
  `--'---'    |  ,   / :  ,      .-./;  |,'  ;  :   .'   \   :  \  |   :    |'--'.     / |  ,   /  
               ---`-'   `--`----'    '--'    |  ,     .-./   | ,'   \   \  /   `--'---'   ---`-'   
                                              `--`---'   `----'      `----'                                                                                                                           
______            _         _____                           _             
| ___ \          | |       |  __ \                         | |            
| |_/ /___  _   _| |_ ___  | |  \/ ___ _ __   ___ _ __ __ _| |_ ___  _ __ 
|    // _ \| | | | __/ _ \ | | __ / _ \ '_ \ / _ \ '__/ _` | __/ _ \| '__|
| |\ \ (_) | |_| | ||  __/ | |_\ \  __/ | | |  __/ | | (_| | || (_) | |   
\_| \_\___/ \__,_|\__\___|  \____/\___|_| |_|\___|_|  \__,_|\__\___/|_|  
"""




stukapre = """
............................................ZZZZZZZZZZZZZZZZO. ....................
........................................ZZZZZZZZZZZZZZZZZZZZZZZZZ..................
.....................................ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ...............
...................................ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ.............
.................................ZZZZZZZZZZZZZ.............ZZZZZZZZZZZZZ...........
................................ZZZZZZZZZZ..........MM.........ZZZZZZZZZZ..........
..............................ZZZZZZZZZ$.............MM..MM......ZZZZZZZZZZ........
.............................ZZZZZZZZZ............. NM....M .M ....ZZZZZZZZ$.......
............................$ZZZZZZZ..............MM,.. MMM..ZM......ZZZZZZZZ......
...........................ZZZZZZZZ...............M...MM.....,M...... ZZZZZZZZ.....
....................... ...ZZZZZZZ................MM..MM...MMM.........ZZZZZZZ.....
..........................ZZZZZZZ................ ....,M .MM............ZZZZZZZ....
.........................=ZZZZZZ,..............MMMMMMM....MM............?ZZZZZZ+...
.........................ZZZZZZZ........... ...MMMMMM......M.............ZZZZZZZ...
.........................ZZZZZZ$...............MMMMMM..... ..............IZZZZZZ...
........................:ZZZZZZ...............MMMMMMM...MMMMMM............ZZZZZZ...
........................ZZZZZZZ...............MMMMMM... MMMMMM............ZZZZZZI..
........................ZZZZZZZ...............MMMMMM.. MMMMMMM............ZZZZZZZ..
........................ZZZZZZZ..............MMMMMMM...MMMMMM ............ZZZZZZI..
....................... :ZZZZZZ......... ...MMMMMMMM..?MMMMMM ............ZZZZZZ...
.........................ZZZZZZ$......... MMMMMMMMMM..MMMMMMD............$ZZZZZZ...
.........................ZZZZZZZ.......MMMMMMMMMMMM ..MMMMMM. ...........ZZZZZZZ...
.........................:ZZZZZZ~... MMMMMMMMMMMN ..,MMMMMMM............7ZZZZZZ~...
..........................ZZZZZZZ...MMMMMMMMMM....MMMMMMMMMM............ZZZZZZZ....
...........................ZZZZZZZ..MMMMMMM=... MMMMMMMMMMMM...........ZZZZZZZ.....
...........................ZZZZZZZZ..MMMM....7MMMMMMMMMMM~............ZZZZZZZZ.....
............................ZZZZZZZZ........?MMMMMMMMM8.............,ZZZZZZZZ.... .
....................... .....ZZZZZZZZZ......+MMMMMMN ..............ZZZZZZZZ$.......
..............................ZZZZZZZZZZ.....~MMMM ..............ZZZZZZZZZ$........
................................ZZZZZZZZZZ....................:ZZZZZZZZZZ....... ..
.................................ZZZZZZZZZZZZZ.............ZZZZZZZZZZZZO...........
.................................. ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ.............
.....................................ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZO... .......... 
........................................ZZZZZZZZZZZZZZZZZZZZZZZZ$..................
....................... .................. .ZZZZZZZZZZZZZZZZZ. ....................
....................... ...........................................................
"""

stuka = stukapre.replace("."," ")


print (wordart1)
print (stuka)




################################################################### Vraag 1

q1pre = input("Houd je van interactie tijdens een voorstelling?\n")
q1 = q1pre.lower()


if q1 == "ja":
    artiesten["KNOT"]+=1
    artiesten["Clarinde"]+=1
    artiesten["Go Short"]+=1

elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Houd je van interactie tijdens een voorstelling?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["KNOT"] += 1
        artiesten["Clarinde"] += 1
        artiesten["Go Short"] += 1



################################################################### Vraag 2

q1pre = input("Vind je muziek voor normies?\n")
q1 = q1pre.lower()


if q1 == "ja":
    artiesten["Mensen Zeggen Dingen"]+=1
    artiesten["Rene Oskam"]+=1
    artiesten["De Nieuwe Oost"]+=1
    artiesten["Mikaela Burch"]+=1
    artiesten["Sabba"]+=1
    artiesten["CTF"]+=1
    artiesten["Studio Figur"]+=1
    artiesten["NNT"]+=1
    artiesten["Tom Lash"]+=1
    artiesten["Go Short"]+=1
    artiesten["KNOT"]+=1
    artiesten["Clarinde"]+=1

elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Vind je muziek voor plebs?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["Mensen Zeggen Dingen"] += 1
        artiesten["Rene Oskam"] += 1
        artiesten["De Nieuwe Oost"] += 1
        artiesten["Mikaela Burch"] += 1
        artiesten["Sabba"] += 1
        artiesten["CTF"] += 1
        artiesten["Studio Figur"] += 1
        artiesten["NNT"] += 1
        artiesten["Tom Lash"] += 1
        artiesten["Go Short"] += 1
        artiesten["KNOT"] += 1
        artiesten["Clarinde"] += 1




################################################################### Vraag 3

q1pre = input("Zoek je een act die creatief met de Nederlandse taal omgaat?\n")
q1 = q1pre.lower()


if q1 == "ja":
    artiesten["Clittenband"]+=1
    artiesten["George Kush Collective"]+=1
    artiesten["Mensen Zeggen Dingen"]+=1
    artiesten["Rene Oskam"]+=1
    artiesten["De Nieuwe Oost"]+=1

elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Zoek je een act die de creatief met de Nederlandse taal omgaat?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["Clittenband"] += 1
        artiesten["George Kush Collective"] += 1
        artiesten["Mensen Zeggen Dingen"] += 1
        artiesten["Rene Oskam"]+=1
        artiesten["De Nieuwe Oost"]+=1






################################################################### Vraag 4

q1pre = input("Houd je van flirten?\n")
q1 = q1pre.lower()


if q1 == "ja":
    artiesten["Clittenband"]+=1
    artiesten["CTF"]+=1

elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Houd je van flirten?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["Clittenband"] += 1
        artiesten["CTF"] += 1

################################################################### Vraag 5

q1pre = input("Houd je van fietsen?\n")
q1 = q1pre.lower()

if q1 == "ja":
    artiesten["Mikaela Burch"] += 1
    artiesten["Niko"] += 1
    artiesten["De Nieuwe Oost"] += 1

elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Houd je van fietsen?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["Mikaela Burch"] += 1
        artiesten["Niko"] += 1
        artiesten["De Nieuwe Oost"] += 1




################################################################### Vraag 6

q1pre = input("Vind je dat een gitaar te weinig snaren heeft?\n")
q1 = q1pre.lower()

if q1 == "ja":
    artiesten["Lamin Kuyateh"] += 1


elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Vind je dat een gitaar te weinig snaren heeft?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["Lamin Kuyateh"] += 1


################################################################### Vraag 7

q1pre = input("Stotter je ook wel eens als je een hemellichaam wilt benoemen in het engels?\n")
q1 = q1pre.lower()

if q1 == "ja":
    artiesten["Moon Moon Moon"] += 1


elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Stotter je ook wel eens als je een hemellichaam wilt benoemen in het engels?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["Moon Moon Moon"] += 1

################################################################### Vraag 8

q1pre = input("Vind jij zomertuinen ook zo out of season?\n")
q1 = q1pre.lower()

if q1 == "ja":
    artiesten["De Nieuwe Oost"] += 1


elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Vind jij zomertuinen ook zo out of season?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["De Nieuwe Oost"] += 1

################################################################### Vraag 9

q1pre = input("Vind jij het leven soms ook een grote poppenkast?\n")
q1 = q1pre.lower()

if q1 == "ja":
    artiesten["Studio Figur"] += 1


elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Vind jij het leven soms ook een grote poppenkasr?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["Studio Figur"] += 1

################################################################### Vraag 10

q1pre = input("Val jij ook altijd halverwege een film in slaap?\n")
q1 = q1pre.lower()

if q1 == "ja":
    artiesten["Go Short"] += 1


elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Val jij ook altijd halverwege een film in slaap?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["Go Short"] += 1

################################################################### Vraag 11

q1pre = input("Heb jij zin in een half uurtje in een kelder?\n")
q1 = q1pre.lower()

if q1 == "ja":
    artiesten["Clarinde"] += 1
    artiesten["Tom Lash"] += 1
    artiesten["Go Short"] += 1
    artiesten["Indianizer"] += 1


elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Heb jij zin in een half uurtje in een kelder?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["Clarinde"] += 1
        artiesten["Tom Lash"] += 1
        artiesten["Go Short"] += 1
        artiesten["Indianizer"] += 1

################################################################### Vraag 12

q1pre = input("Houd je van cabaret?\n")
q1 = q1pre.lower()

if q1 == "ja":
    artiesten["Mikaela Burch"] += 1
    artiesten["Tom Lash"] += 1



elif str(q1) != "ja" and str(q1) != "nee":
    print("Antwoord met een Ja of een Nee")
    q1pre = input("Houd je can cabaret?\n")
    q1 = q1pre.lower()

    if q1 == "ja":
        artiesten["Mikaela Burch"] += 1
        artiesten["Tom Lash"] += 1


##############################################RESULT

i=0
winning_set = []

while i < 3:
    i+=1
    result = max(artiesten.items(), key=operator.itemgetter(1))[0]
    winning_set.append(result)
    del artiesten[result]


print(winning_set)



url = "https://www.google.com/maps/dir/?api=1&origin="+str(locaties[winning_set[0]])+"&destination=waalbanddijk+14+nijmegen&waypoints="+str(locaties[winning_set[1]])+"|"+str(locaties[winning_set[2]])+"&travelmode=bicycling"
webbrowser.open_new(url)

time.sleep(0.5)

webbrowser.open_new_tab("https://www.stukafest.nl/nijmegen/acts/"+str(sitelinks[winning_set[0]]))
webbrowser.open_new_tab("https://www.stukafest.nl/nijmegen/acts/"+str(sitelinks[winning_set[1]]))
webbrowser.open_new_tab("https://www.stukafest.nl/nijmegen/acts/"+str(sitelinks[winning_set[2]]))
#webbrowser.open_new_tab("https://meh.com")